<?php
require_once("Bundle.php");

Class Item 
{
    public  $id;
    public  $name; 
    public  $quantity;
    
    private $pdo;

    public function __construct()
    {
        $this->pdo = connect();
    } 

    public function changeQuantity($quantity)
    {
        if(isset($this->id))
        {
            $query = $this->pdo->prepare("UPDATE item SET quantity = :quantity WHERE id = :id");
            $query->execute(["quantity"=>$quantity, "id"=>$this->id]); 
        } else {
            return 1;
        }
    }

    public function writeItem()
    {
        if(isset($this->name))
        {
            if(!isset($this->quantity))
            {
                $this->quantity = 0; 
            }
           $query = $this->pdo->prepare("INSERT INTO item (name, quantity) VALUES(:name, :quantity)");
           $query->execute(["name"=>$this->name, "quantity"=>$this->quantity]);     
        } else {
            return 1;
        }
    }
}
?>
